<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\pertanyaanController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('pertanyaan', [pertanyaanController::class, 'index']);
//Route::get('pertanyaan/create', [PertanyaanController::class, 'create']);
Route::view('pertanyaan/create', 'create');
Route::post('pertanyaan', [pertanyaanController::class, 'store']);
/*
Route::get('pertanyaan/{id}', function ($id) {
    return 'User '.$id;
})
*/
Route::get('pertanyaan/{id}', [pertanyaanController::class, 'show']);
Route::get('pertanyaan/{id}/edit', [pertanyaanController::class, 'edit']);
Route::put('pertanyaan/{id}', [pertanyaanController::class, 'update']);
Route::delete('pertanyaan/{id}', [pertanyaanController::class, 'destroy']);