<style>
    th, td 
    {
      border: 1px solid black;
    }
    a
    {
      text-decoration: none;
    }
    </style>

<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
      <button><a href="{{url("pertanyaan/create")}} ">Insert Data</a></button>
      <button><a href="{{url("pertanyaan")}}">home</a></button>
      <hr>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Isi</th>
          <th>Dibuat</th>
          <th>Diedit</th>
          <th>Pembuat</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
        </thead>
        @php $s=1; @endphp
        @foreach ($pertanyaan as $item)
        <tbody>
            <tr>
                <td style="text-align: center">a{{ $s++ }} </td>
                <td><a href="{{url('pertanyaan/'. $item->id)}} ">{{$item->judul}}</a></td>
                <td>{{$item->isi}}</td>
                <td>{{$item->tanggal_dibuat}} </td>
                <td>{{$item->tanggal_diperbarui}}</td>
                <td>{{$item->profil_id}}</td>
                <td><a href="{{url("pertanyaan/". $item->id . "/edit")}} ">Edit</a></td>
                <td style="text-align: center">
                <form action="/pertanyaan/{{$item->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <input type="submit" value="X">
              </form>
                </td>
            </tr>
        </tbody>
        @endforeach
      </table>
    </div>
    <!-- /.card-body -->
  </div>