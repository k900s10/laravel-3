<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class pertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
    //    dd($pertanyaan);
        return view('pertanyaan', compact('pertanyaan'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

//        DB::insert('insert into pertanyaan (judul, isi) values (?, ?)', [$judul, $isi]);
        
        DB::table('pertanyaan')
            ->insert([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('pertanyaan');
    }

    public function show(Request $request)
    {
        $id = $request->id;
        $pertanyaan = DB::select('select * from pertanyaan where id = ?', [$id]);
        return view('pertanyaan', ['pertanyaan' => $pertanyaan]);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $pertanyaan = DB::select('select * from pertanyaan where id = ?', [$id]);
        return view('edit', ['pertanyaan' => $pertanyaan]);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        //dd($judul . $isi);
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);

        $pertanyaan = DB::select('select * from pertanyaan where id = ?', [$id]);
        return view('pertanyaan', ['pertanyaan' => $pertanyaan]);
    }

    public function destroy($id)
    {
        DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('pertanyaan');
    }

}
